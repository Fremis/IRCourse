#!flask/bin/python
import json
import os
from functools import lru_cache

import requests

from flask.ext.cors import CORS
from flask import Flask
from flask import request
from flask import render_template

from spellchecker_with_deletes import Profiler, Corrector
from logger import Logger

from ranking import passage_score
from snippet import get_snippet
from collections import defaultdict

app = Flask(__name__)
CORS(app)

current_path = os.getcwd()
release = True

@app.route('/', methods=['GET', 'POST'])
@app.route('/index/', methods=['GET', 'POST'])
def index():
    return render_template('index.html')

# @app.route('/ranks', methods=['GET', 'POST'])
# def assessor():
#     return render_template('assessor_form.html')

@app.route('/save/', methods=['POST'])
def save_ranks():
    ranked_json = request.json
    try:
        with open(current_path + '\\logs\\ranks\\assessorRanks.json', 'w', encoding='utf-8') as f:
            json.dump(ranked_json, f, ensure_ascii=False, indent=4, separators=(',', ': '))
        return json.dumps({"message": "Your results are saved, thanks!"})
    except Exception:
        return json.dumps({"message": Exception})


@app.route('/correct/<query>', methods=['GET'])
def correcting(query):
    print("Input: %s" % query)
    with Profiler() as p:
        if release:
            corrected = ' '.join(corrector.correct(query))
        else:
            corrected = query
        print("Corrected to => %s" % corrected)
    data = get_ranked_results(corrected)
    #print(data)
    if 'docs' in data:
        logger.get_log(corrected, data)
        logger.dump_logs()
    return json.dumps(data, ensure_ascii=False)


def get_positions_from_index(query_info):
    docs_title = defaultdict(list)
    docs_desc = defaultdict(list)
    for record in query_info:
        word = record['word'].lower()
        word_docs = record['docs']
        for word_doc in word_docs:
            doc_id = word_doc['docId']
            positions = word_doc['pos']
            title_positions = []
            desc_positions = []
            for pos in positions:
                if pos < 200:
                    title_positions.append(pos)
                else:
                    desc_positions.append(pos-200)
            if title_positions:
                docs_title[doc_id].append((word, title_positions))
            if desc_positions:
                docs_desc[doc_id].append((word, desc_positions))
    return docs_title, docs_desc

@lru_cache()
def get_ranked_results(corrected_query):
    result = []
    url = 'http://fremis.ddns.net/iwa/SearchService.svc/find'
    parameters = {'query': corrected_query}
    with Profiler() as p:
        print("index")
        r = requests.get(url, params=parameters)
        data = r.json()
    documents = data['documents']
    query_info = data['query']

    title_poses, desc_poses = get_positions_from_index(query_info)
    #print(title_poses)
    #print(desc_poses)
    if not documents:
        print('pls')
        return {"message": "По вашему запросу ничего не найдено"}
    with Profiler() as p:
        print("range")
        for document in documents:
            snippet = get_snippet(corrected_query, document['description'], 315)
            score = passage_score(corrected_query, title_poses, desc_poses, document, snippet)
            result.append({'title': document['title'],
                           'snippet': snippet,
                           'link': document['link'],
                           'magnet': document['magnet'],
                           'score': score,
                           'category': document['category'],
                           'docID': document['id']})
    result.sort(key=lambda document: document['score'], reverse=True)
    return {'docs': result, 'query': corrected_query}

if __name__ == '__main__':
    with Profiler() as p:
        if release:
            corrector = Corrector()
        logger = Logger()
        print("Server is launched")

    if release:
        app.run(host='0.0.0.0')
    else:
        app.run(host='0.0.0.0', debug=True)
