import json
import os
import datetime
import random


class Logger:
    current_path = os.getcwd()

    def __init__(self):
        self.logs = []

    def get_log(self, query, data):
        next_doc = random.randint(5, 10)
        if len(data['docs']) >= next_doc:
            query_log = {"query": query,
                         "docs": [
                             {
                                 "docID": data['docs'][0]['docID'],
                                 "title": data['docs'][0]['title'],
                                 "url": data['docs'][0]['link'],
                                 "body": data['docs'][0]['snippet'],
                                 "rank": 0
                             },
                             {
                                 "docID": data['docs'][next_doc]['docID'],
                                 "title": data['docs'][next_doc]['title'],
                                 "url": data['docs'][next_doc]['link'],
                                 "body": data['docs'][next_doc]['snippet'],
                                 "rank": 0
                             }
                         ]
                         }
            self.logs.append(query_log)
        print(self.logs)
        # with open('logs.json', 'a', encoding='utf-8') as f:
        # json.dump(query, f, ensure_ascii=False, indent=4, separators=(',', ': '))

    def dump_logs(self):
        now = datetime.datetime.now().strftime("%Y-%m-%d-%H")
        with open(self.current_path + '\\logs\\query\\' + now + '.json', 'w', encoding='utf-8') as f:
            json.dump(self.logs, f, ensure_ascii=False, indent=4, separators=(',', ': '))
            print('logs dumped')

    def count_stats(self):
        with open(self.current_path + '\\logs\\ranks\\assessorRanks.json', 'r', encoding='utf-8') as f:
            ranks = json.load(f)
        success = 0
        all_results = len(ranks)
        for ranked_result in ranks:
            if ranked_result['docs'][0]['rank'] == 1:
                success += 1
        return success/all_results

if __name__ == '__main__':
    logger = Logger()
    print(logger.count_stats())