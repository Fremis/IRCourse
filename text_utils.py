import re
from collections import defaultdict

MAX_VALUE = 100000000000


def split_to_sentences(text):
    regex = re.compile("""
    (?:
        (?:
            (?<!\\d(?:р|г|к))
            (?<!и\\.т\\.(?:д|п))
            (?<!и(?=\\.т\\.(?:д|п)\\.))
            (?<!и\\.т(?=\\.(?:д|п)\\.))
            (?<!руб|коп)
        \\.) |
        [!?\\n]
    )+
    """, re.X)
    res = []
    sear = regex.search(text)
    while sear:
        res.append(text[:sear.end()])
        text = text[sear.end():]
        sear = regex.search(text)
    res.append(text)
    return res


def tokenize(line, split_symbol=' '):
    extra_symbols = '''™„…°×/%+@$№•—−–©“”[]*(){}`'«» .,!?-\n\r\t:;\xa0"'''
    line = line.replace('\n', ' ')
    return list(filter(lambda x: len(x) > 0,
                       (token.strip(extra_symbols).lower()
                        for token in line.split(split_symbol))))


def get_positions(query_tokens, text_tokens):
    index = defaultdict(list)

    for pos, token in enumerate(text_tokens):
        if token in query_tokens:
            index[token].append(pos)
    return index
