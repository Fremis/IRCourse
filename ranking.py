from collections import defaultdict
from text_utils import tokenize
import math

MAX_VALUE = 100000000


def argmin(pos, v):
    min_val = MAX_VALUE
    arg = -1
    for i in range(len(v)):
        if v[i][pos[i]] < min_val:
            min_val = v[i][pos[i]]
            arg = i
    return arg


def argmax(pos, v):
    max_val = -1
    arg = -1
    for i in range(len(v)):
        if v[i][pos[i]] > max_val:
            max_val = v[i][pos[i]]
            arg = i
    return arg


def list_max(pos, v):
    arg = argmax(pos, v)
    return v[arg][pos[arg]]


def minwindow(positions):
    if not positions:
        return -1, -1, []

    pos = []
    num_to_term = {}

    for i, term_info in enumerate(positions):
        term, poses = term_info
        pos.append(poses+[MAX_VALUE])
        num_to_term[i] = term

    m = len(positions)
    cur_pos = [0 for _ in range(m)]
    best_pos = cur_pos[:]
    best_window = MAX_VALUE

    while True:
        min_list = argmin(cur_pos, pos)
        if min_list == -1:
            break
        min_val = pos[min_list][cur_pos[min_list]]
        max_val = list_max(cur_pos, pos)
        if max_val-min_val < best_window:
            best_window = max_val-min_val
            best_pos = cur_pos[:]
        cur_pos[min_list] += 1

    final_poses = [pos[i][best_pos[i]] for i in range(m)]
    l = min(final_poses)
    r = l+best_window

    if l == MAX_VALUE:
        return -1, -1, final_poses
    else:
        return l, r, final_poses


def get_positions(query, text):
    index = defaultdict(list)

    for pos, token in enumerate(text):
        if token in query:
            index[token].append(pos)
    return index


def calc_metrics(q_poses, query_len, text_len):
    l, r, best_poses = minwindow(q_poses)
    print(l, r, best_poses)
    if l == -1:
        return 0, 0, 0
    query_unique_count = len(best_poses)
    proximity = 1/(l+1)
    window_len = r-l+1
    query_terms_ratio = query_unique_count/window_len
    sorted_poses = list(sorted(best_poses))
    positions_match = 0

    for i in range(query_unique_count):
        if sorted_poses[i] == best_poses[i]:
            positions_match += 1
    match_ratio = positions_match/query_unique_count
    return proximity, query_terms_ratio, match_ratio


rutracker_opened = 1072915200
max_date = 1464220799-rutracker_opened  # 26 may 2016


def passage_score(query, title_poses, desc_poses, document, snippet):
    bm25_rank = document['rank']
    cosine = document['cos']

    title = document['title']
    description = document['description']

    #title_tokens = tokenize(title)
    title_len = document['tlen']
    #desc_tokens = tokenize(description)
    desc_len = document['len']
    doc_id = document['id']

    query_tokens = tokenize(query)
    # t_poses = get_positions(query_tokens, title_tokens)
    # d_poses = get_positions(query_tokens, desc_tokens)
    # print(t_poses)
    # print(d_poses)
    #
    t_proximity, t_query_terms_ratio, t_match_ratio = calc_metrics(title_poses[doc_id], len(query_tokens), title_len)
    #print(t_proximity, t_query_terms_ratio, t_match_ratio)
    d_proximity, d_query_terms_ratio, d_match_ratio = calc_metrics(desc_poses[doc_id], len(query_tokens), desc_len)
    #print(d_proximity, d_query_terms_ratio, d_match_ratio)
    keygen = document['keygen']
    lang = document['lang']
    qual = document['qual']#*1.5

    title_score = t_proximity+t_query_terms_ratio+t_match_ratio
    desc_score = d_proximity+d_query_terms_ratio+d_match_ratio
    pages_count = document['pages']
    creation_date = int(document['date'][6:-2])/1000-rutracker_opened

    fresh_ratio = creation_date/max_date
    print(document['title'], t_proximity, t_query_terms_ratio, t_match_ratio,
          d_proximity, d_query_terms_ratio, d_match_ratio, document['qual'], fresh_ratio)
    if not snippet:
        desc_score -= 3

    stupid_score = bm25_rank+cosine
    passage_scores = title_score+desc_score*0.3
    quality_score = (keygen+lang+qual+math.log(pages_count))/3
    return stupid_score+passage_scores+quality_score+fresh_ratio
