import os
import re
import math
from scipy.stats import poisson
import time
import json
import pickle as p

max_edit_distance = 2
verbose = 2


def get_deletes_list(w, max_edit_distance):
    """
    Given a word, derive strings with up to max_edit_distance
    characters deleted.
    """
    deletes = []
    queue = [w]
    for d in range(max_edit_distance):
        temp_queue = []
        for word in queue:
            if len(word) > 1:
                for c in range(len(word)):  # character index
                    word_minus_c = word[:c] + word[c + 1:]
                    if word_minus_c not in deletes:
                        deletes.append(word_minus_c)
                    if word_minus_c not in temp_queue:
                        temp_queue.append(word_minus_c)
        queue = temp_queue

    return deletes


def create_dictionary_entry(w, dictionary, max_edit_distance):
    """
    Add a word and its derived deletions to the dictionary.
    Dictionary entries are of the form:
    ([list of suggested corrections], frequency of word in corpus)
    """

    new_real_word_added = False

    # check if word is already in dictionary
    if w in dictionary:
        # increment count of word in corpus
        dictionary[w] = (dictionary[w][0], dictionary[w][1] + 1)
    else:
        # create new entry in dictionary
        dictionary[w] = ([], 1)

    if dictionary[w][1] == 1:

        new_real_word_added = True
        deletes = get_deletes_list(w, max_edit_distance)

        for item in deletes:
            if item in dictionary:
                # add (correct) word to delete's suggested correction
                # list if not already there
                if item not in dictionary[item][0]:
                    dictionary[item][0].append(w)
            else:
                dictionary[item] = ([w], 0)

    return new_real_word_added


def pre_processing(max_edit_distance=2):
    """
    Load json files to create a dictionaries if no models
    were found.
    """

    dictionary = dict()
    start_prob = dict()
    transition_prob = dict()
    word_count = 0
    transitions = 0

    current_path = os.getcwd()
    file_list = os.listdir(current_path + '\\index\\')
    for fname in file_list:
        with open(current_path + '\\index\\' + fname, 'r', encoding='utf-8') as file:
            docs = json.load(file)
            for doc in docs:
                # separate by words by non-alphabetical characters
                words_title = re.findall('(\w+([\'-]\w+)?)', doc['title'].lower())
                words = list(map(lambda match: match[0], words_title))
                if doc['pages'] > 1 and doc['description']:
                    words_body = re.findall('(\w+([\'-]\w+)?)', doc['description'].lower())[:20]
                    words += list(map(lambda match: match[0], words_body))
                for w, word in enumerate(words):
                    word = word.strip('\'-')
                    # create/update dictionary entry
                    if create_dictionary_entry(
                            word, dictionary, max_edit_distance):
                        word_count += 1

                    # update probabilities for Hidden Markov Model
                    if w == 0:

                        # probability of a word being at the
                        # beginning of a sentence
                        if word in start_prob:
                            start_prob[word] += 1
                        else:
                            start_prob[word] = 1
                    else:

                        # probability of transitionining from one
                        # word to another
                        # dictionary format:
                        # {previous word: {word1 : P(word1|prevous
                        # word), word2 : P(word2|prevous word)}}

                        # check whether prior word is present
                        # - create if not
                        if words[w - 1] not in transition_prob:
                            transition_prob[words[w - 1]] = dict()

                        # check whether current word is present
                        # - create if not
                        if word not in transition_prob[words[w - 1]]:
                            transition_prob[words[w - 1]][word] = 0

                        # update value
                        transition_prob[words[w - 1]][word] += 1
                        transitions += 1
        print('file\'s done')
    # convert counts to log-probabilities, to avoid underflow in
    # later calculations (note: natural logarithm, not base-10)

    # also calculate (small) default probabilities for words that
    # have not already been seen

    # probability of a word being at the beginning of a sentence
    total_start_words = float(sum(start_prob.values()))
    default_start_prob = math.log(1 / total_start_words)
    start_prob.update(
        {k: math.log(v / total_start_words)
         for k, v in start_prob.items()})

    # probability of transitioning from one word to another
    default_transition_prob = math.log(1. / transitions)
    transition_prob.update(
        {k: {k1: math.log(float(v1) / sum(v.values()))
             for k1, v1 in v.items()}
         for k, v in transition_prob.items()})

    # output summary statistics
    print('Total unique words in corpus: %i' % word_count)
    print('Total items in dictionary: %i' % len(dictionary))
    print('  Edit distance for deletions: %i' % max_edit_distance)
    print('Total unique words at the start of a sentence: %i' % len(start_prob))
    print('Total unique word transitions: %i' % len(transition_prob))

    return dictionary, start_prob, default_start_prob, transition_prob, default_transition_prob


def dameraulevenshtein(s1, s2):
    if len(s1) < len(s2):
        return dameraulevenshtein(s2, s1)

    # len(s1) >= len(s2)
    if len(s2) == 0:
        return len(s1)

    previous_row = range(len(s2) + 1)
    for i, c1 in enumerate(s1):
        current_row = [i + 1]
        for j, c2 in enumerate(s2):
            insertions = previous_row[
                             j + 1] + 1  # j+1 instead of j since previous_row and current_row are one character longer
            deletions = current_row[j] + 1  # than s2
            substitutions = previous_row[j] + (c1 != c2)
            current_row.append(min(insertions, deletions, substitutions))
        previous_row = current_row

    return previous_row[-1]


def get_suggestions(string, dictionary):
    global verbose
    suggest_dict = {}
    min_suggest_len = float('inf')

    queue = [string]
    q_dictionary = {}  # items other than string that we've checked

    while len(queue) > 0:
        q_item = queue[0]  # pop
        queue = queue[1:]

        # early exit
        if ((verbose < 2) and (len(suggest_dict) > 0) and
                ((len(string) - len(q_item)) > min_suggest_len)):
            break

        # process queue item
        if (q_item in dictionary) and (q_item not in suggest_dict):
            if (dictionary[q_item][1] > 0):
                # word is in dictionary, and is a word from the corpus, and
                # not already in suggestion list so add to suggestion
                # dictionary, indexed by the word with value (frequency in
                # corpus, edit distance)
                # note q_items that are not the input string are shorter
                # than input string since only deletes are added (unless
                # manual dictionary corrections are added)
                assert len(string) >= len(q_item)
                suggest_dict[q_item] = (dictionary[q_item][1],
                                        len(string) - len(q_item))
                # early exit
                if ((verbose < 2) and (len(string) == len(q_item))):
                    break
                elif (len(string) - len(q_item)) < min_suggest_len:
                    min_suggest_len = len(string) - len(q_item)

            # the suggested corrections for q_item as stored in
            # dictionary (whether or not q_item itself is a valid word
            # or merely a delete) can be valid corrections
            for sc_item in dictionary[q_item][0]:
                if (sc_item not in suggest_dict):

                    # compute edit distance
                    # suggested items should always be longer
                    # (unless manual corrections are added)
                    assert len(sc_item) > len(q_item)

                    # q_items that are not input should be shorter
                    # than original string
                    # (unless manual corrections added)
                    assert len(q_item) <= len(string)

                    if len(q_item) == len(string):
                        assert q_item == string
                        item_dist = len(sc_item) - len(q_item)

                    # item in suggestions list should not be the same as
                    # the string itself
                    assert sc_item != string

                    # calculate edit distance using, for example,
                    # Damerau-Levenshtein distance
                    item_dist = dameraulevenshtein(sc_item, string)

                    # do not add words with greater edit distance if
                    # verbose setting not on
                    if ((verbose < 2) and (item_dist > min_suggest_len)):
                        pass
                    elif item_dist <= max_edit_distance:
                        assert sc_item in dictionary  # should already be in dictionary if in suggestion list
                        suggest_dict[sc_item] = (dictionary[sc_item][1], item_dist)
                        if item_dist < min_suggest_len:
                            min_suggest_len = item_dist

                    # depending on order words are processed, some words
                    # with different edit distances may be entered into
                    # suggestions; trim suggestion dictionary if verbose
                    # setting not on
                    if verbose < 2:
                        suggest_dict = {k: v for k, v in suggest_dict.items() if v[1] <= min_suggest_len}

        # now generate deletes (e.g. a substring of string or of a delete)
        # from the queue item
        # as additional items to check -- add to end of queue
        assert len(string) >= len(q_item)

        # do not add words with greater edit distance if verbose setting
        # is not on
        if ((verbose < 2) and ((len(string) - len(q_item)) > min_suggest_len)):
            pass
        elif (len(string) - len(q_item)) < max_edit_distance and len(q_item) > 1:
            for c in range(len(q_item)):  # character index
                word_minus_c = q_item[:c] + q_item[c + 1:]
                if word_minus_c not in q_dictionary:
                    queue.append(word_minus_c)
                    q_dictionary[word_minus_c] = None  # arbitrary value, just to identify we checked this

    as_list = suggest_dict.items()
    out_list = sorted(as_list, key=lambda term: (term[1][1], -term[1][0]))

    if len(out_list) == 0:
        # to ensure Viterbi can keep running
        # use the word itself if no corrections are found
        return [(string, 0)]

    if verbose == 2:
        return [(i[0], i[1][1]) for i in out_list[:10]]
    else:
        return [(i[0], i[1][1]) for i in out_list]


def get_emission_prob(edit_dist, poisson_lambda=0.01):
    """
    The emission probability, i.e. P(observed word|intended word)
    is approximated by a Poisson(k, l) distribution, where 
    k=edit distance between the observed word and the intended
    word and l=0.01.

    Both the overall approach and the parameter of l=0.01 are based on
    the 2015 lecture notes from AM207 Stochastic Optimization.
    Various parameters for lambda between 0 and 1 were tested, which
    confirmed that 0.01 yields the most accurate word suggestions.
    """

    return math.log(poisson.pmf(edit_dist, poisson_lambda))


def get_start_prob(word, start_prob, default_start_prob):
    """
    P(word being at the beginning of a sentence)
    """
    try:
        return start_prob[word]
    except KeyError:
        return default_start_prob


def get_transition_prob(cur_word, prev_word,
                        transition_prob, default_transition_prob):
    """
    P(word|previous word)
    """
    try:
        return transition_prob[prev_word][cur_word]
    except KeyError:
        return default_transition_prob


def get_path_prob(prev_word, prev_path_prob):
    """
    P(previous path)
    """
    try:
        return prev_path_prob[prev_word]
    except KeyError:
        return math.log(math.exp(min(prev_path_prob.values())) / 2.)


def viterbi(words, dictionary, start_prob, default_start_prob,
            transition_prob, default_transition_prob):
    """
    Determine the most likely (intended) sequence, based on the
    observed sequence.
    """

    v = [{}]
    path = {}

    # character level correction - used to determine state space
    corrections = get_suggestions(words[0], dictionary)

    # Initialize base cases (first word in the sentence)
    for sug_word in corrections:
        # compute the value for all possible starting states
        v[0][sug_word[0]] = math.exp(
            get_start_prob(sug_word[0], start_prob,
                           default_start_prob)
            + get_emission_prob(sug_word[1]))

        # remember all the different paths (only one word so far)
        path[sug_word[0]] = [sug_word[0]]

    # normalize for numerical stability
    path_temp_sum = sum(v[0].values())
    v[0].update({k: math.log(v / path_temp_sum)
                 for k, v in v[0].items()})

    # keep track of previous state space
    prev_corrections = [i[0] for i in corrections]

    # return if the sentence only has one word
    if len(words) == 1:
        try:
            path_context = [max(v[0], key=lambda i: v[0][i])]
            return path_context
        except ValueError:
            return "no matches found"

    # run Viterbi for all subsequent words in the sentence
    for t in range(1, len(words)):

        v.append({})
        new_path = {}

        # character level correction
        corrections = get_suggestions(words[t], dictionary)

        for sug_word in corrections:
            sug_word_emission_prob = get_emission_prob(sug_word[1])

            # compute the probabilities associated with all previous
            # states (paths), only keep the maximum
            (prob, word) = max(
                (get_path_prob(prev_word, v[t - 1])
                 + get_transition_prob(sug_word[0], prev_word,
                                       transition_prob, default_transition_prob)
                 + sug_word_emission_prob, prev_word)
                for prev_word in prev_corrections)

            # save the maximum probability for each state
            v[t][sug_word[0]] = math.exp(prob)

            # store the full path that results in this probability
            new_path[sug_word[0]] = path[word] + [sug_word[0]]

        # normalize for numerical stability
        path_temp_sum = sum(v[t].values())
        v[t].update({k: math.log(v / path_temp_sum)
                     for k, v in v[t].items()})

        # keep track of previous state space
        prev_corrections = [i[0] for i in corrections]

        # don't need to remember the old paths
        path = new_path

    # after all iterations are completed, look up the word with the
    # highest probability
    (prob, word) = max((v[t][sug_word[0]], sug_word[0]) for sug_word in corrections)
    path_context = path[word]
    return path_context
    # look up the full path associated with this word


class Profiler(object):
    def __enter__(self):
        self._startTime = time.clock()

    def __exit__(self, type, value, traceback):
        print("Elapsed time: {:.8f} sec".format(time.clock() - self._startTime))


# if __name__ == '__main__':
#     """
#     tests
#     """
#     print('Pre-processing')
#
#     start_time = time.time()
#
#     dictionary, start_prob, default_start_prob, \
#     transition_prob, default_transition_prob \
#         = pre_processing()
#
#     run_time = time.time() - start_time
#
#     print('-----')
#     print('%.2f seconds to run' % run_time)
#     print('-----')
#
#     start_time = time.time()
#     '''
#     correct_phrase(check_file, dictionary,
#                    start_prob, default_start_prob,
#                    transition_prob, default_transition_prob)'''
#
#     while True:
#         words = input()
#         with Profiler() as p:
#             print(viterbi(words.lower().split(), dictionary,
#                           start_prob, default_start_prob,
#                           transition_prob, default_transition_prob))


class Corrector():
    def __init__(self):
        print("Looking for models to load...")
        try:
            self.deserialize()
            print("Models have been loaded.")
        except FileNotFoundError:
            print("Models have not been found, starting training.")
            self.dictionary, self.start_prob, self.default_start_prob, \
                self.transition_prob, self.default_transition_prob \
                = pre_processing()
            self.serialize()

    def correct(self, phrase):
        words = phrase.lower().split()
        return viterbi(words, self.dictionary,
                       self.start_prob, self.default_start_prob,
                       self.transition_prob, self.default_transition_prob)

    def serialize(self):
        current_path = os.getcwd()
        with open(current_path + '\\models\\dictionary.p', 'wb') as open_file:
            p.dump(self.dictionary, open_file, protocol=-1)
        with open(current_path + '\\models\\start_prob.p', 'wb') as open_file:
            p.dump(self.start_prob, open_file, protocol=-1)
        with open(current_path + '\\models\\default_start_prob.p', 'wb') as open_file:
            p.dump(self.default_start_prob, open_file, protocol=-1)
        with open(current_path + '\\models\\transition_prob.p', 'wb') as open_file:
            p.dump(self.transition_prob, open_file, protocol=-1)
        with open(current_path + '\\models\\default_transition_prob.p', 'wb') as open_file:
            p.dump(self.default_transition_prob, open_file, protocol=-1)

    def deserialize(self):
        current_path = os.getcwd()
        with open(current_path + '\\models\\dictionary.p', 'rb') as open_file:
            self.dictionary = p.load(open_file)
        with open(current_path + '\\models\\start_prob.p', 'rb') as open_file:
            self.start_prob = p.load(open_file)
        with open(current_path + '\\models\\default_start_prob.p', 'rb') as open_file:
            self.default_start_prob = p.load(open_file)
        with open(current_path + '\\models\\transition_prob.p', 'rb') as open_file:
            self.transition_prob = p.load(open_file)
        with open(current_path + '\\models\\default_transition_prob.p', 'rb') as open_file:
            self.default_transition_prob = p.load(open_file)
