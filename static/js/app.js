angular.module('irsearch', ['ngAnimate', 'ui.bootstrap', 'angularUtils.directives.dirPagination', 'ngSanitize']);

angular.module('irsearch').config(['$interpolateProvider', '$compileProvider', function ($interpolateProvider, $compileProvider) {
    $interpolateProvider.startSymbol('{[');
    $interpolateProvider.endSymbol(']}');
    $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|chrome-extension|magnet):/);
}]);



angular.module('irsearch').controller('ResultsController', ['$http', function ($http) {
    var output = this;
    output.userSearched = false;
    output.results = [];
    output.phrase = "";
    output.correctedQuery = "";
    output.errors = "";
    output.filters = {};
    this.showResults = function () {
        $http.get("http://fremis.ddns.net:5000/correct/" + output.phrase).success(function (json) {
            output.userSearched = true;
            output.input = output.phrase;
            console.log(json);
            output.results = json.docs;
            output.correctedQuery = json.query;
            output.errors = json.message;
        })
    };
}]);


angular.module('irsearch').controller('TabController', function () {
    this.tab = 1;

    this.setTab = function (newValue) {
        this.tab = newValue;
    };

    this.isSet = function (tabName) {
        return this.tab === tabName;
    };
});