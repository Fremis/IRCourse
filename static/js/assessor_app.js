/**
 * Created by Inso on 21.05.2016.
 */
angular.module('assesorform', ['ngAnimate', 'ui.bootstrap', 'angularUtils.directives.dirPagination', 'ngSanitize']);
angular.module('assesorform').config(['$interpolateProvider', function ($interpolateProvider) {
    $interpolateProvider.startSymbol('{[');
    $interpolateProvider.endSymbol(']}');
}]);
angular.module('assesorform').controller('AssessorController', ['$http', function ($http) {
    var output = this;
    output.results = [];
    output.userReady = false;

    this.getResults = function () {
        $http.get("logs/query/assessor_samples.json").success(function (json) {
            output.userReady = true;
            console.log(json);
            output.results = json;
        })
    };

    this.rankDoc = function (doc1, doc2) {
        doc1['rank'] = 1;
        doc2['rank'] = -1;

    };

    this.saveRanks = function () {
        $http.post("http://fremis.ddns.net:5000/save/", output.results).success(function (data) {
            console.log('Success');
            alert(data.message)
        })
    };
}]);