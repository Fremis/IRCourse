import os
import string
from collections import *
import pickle

import time

from Parser import StatsParser


def filter_dicts(self):
    self.term_counter = {k: v for k, v in self.term_counter.items() if v > 3}
    self.bigram_counter = {k: v for k, v in self.bigram_counter.items() if v > 2}
    self.trigram_counter = {k: v for k, v in self.trigram_counter.items() if v > 2}


class profiler(object):
    def __enter__(self):
        self._startTime = time.clock()

    def __exit__(self, type, value, traceback):
        print("Elapsed time: {:.8f} sec".format(time.clock() - self._startTime))


class Trainer:

    def __init__(self, n):
        self.term_counter = defaultdict(int)
        self.bigram_counter = defaultdict(int)
        self.trigram_counter = defaultdict(int)
        self.n = n
        self.bigram_model = defaultdict(dict)
        self.trigram_model = defaultdict(dict)
        self.extra_symbols = '''„…°×/%+@$1234567890№•—−–©“”[]*(){}`'«» .,!?-\n\r\t:;\xa0"'''
        self.parser = StatsParser()

    @staticmethod
    def bigram_finder(tokens):
        return (' '.join((tokens[i], tokens[i + 1])) for i in range(len(tokens) - 1))

    @staticmethod
    def trigram_finder(tokens):
        return (' '.join((tokens[i], tokens[i + 1], tokens[i + 2])) for i in range(len(tokens) - 2))

    def get_grams(self, tokens):
        for i in range(len(tokens) - (self.n - 1)):
            bigram = tuple(tokens[i:i + self.n - 1])
            trigram = tuple(tokens[i:i + self.n])
            if not ('@' in trigram and trigram[0] != '@'):
                yield bigram, trigram

    def get_counters_from_file(self):
        current_path = os.getcwd()
        tokens = []
        filename = current_path + '\\corpus\\big_news.txt'
        tokens += self.get_tokens_from_txt(filename)
        bigrams = self.bigram_finder(tokens)
        trigrams = self.trigram_finder(tokens)
        self.update_counters(tokens, bigrams, trigrams)

    def update_counters(self, tokens, bigrams, trigrams):
        for token in tokens:
            self.term_counter[token] += 1
        for bigram in bigrams:
            self.bigram_counter[bigram] += 1
        for trigram in trigrams:
            self.trigram_counter[trigram] += 1

    def get_counters_from_files(self):
        current_path = os.getcwd()
        tokens = []
        for i in range(1):
            file_list = os.listdir(current_path + '\\corpus\\RutrackerGames\\' + str(i) + '\\')
            for file in file_list:
                if file:
                    filename = current_path + '\\corpus\\RutrackerGames\\' + str(i) + '\\' + file
                    tokens += self.get_tokens(filename)
            self.train(tokens)
            tokens.clear()
            print('Folder parsed')
        self.build_model()

    def get_tokens_from_txt(self, filename):
        all_tokens = []
        try:
            with open(filename, 'r', encoding='utf-8') as f:
                for raw_line in f:
                    tokens = list(filter(lambda x: len(x) > 0,
                                         (token.strip(self.extra_symbols).lower()
                                          for token in raw_line.split())))
                    all_tokens += tokens  # + ['#']
            return all_tokens
        except AttributeError or TypeError:
            print(f)

    def get_tokens(self, filename):
        all_tokens = []
        with open(filename, 'r') as f:
            self.parser.parse(f)
            for raw_line in self.parser.body:
                tokens = list(filter(lambda x: len(x) > 0,
                                     (token.strip(self.extra_symbols).lower()
                                      for token in raw_line.split())))
                all_tokens += tokens  # + ['#']
        return all_tokens

    def train(self, tokens):
        for token in tokens:
            self.term_counter[token] += 1
        for nm1gram, ngram in self.get_grams(tokens):
            self.bigram_counter[nm1gram] += 1
            self.trigram_counter[ngram] += 1

    def build_model(self):
        for (word1, word2), freq in self.bigram_counter.items():
            self.bigram_model[word1][word2] = freq/self.term_counter[word1]
        for (word1, word2, word3), freq in self.trigram_counter.items():
            self.trigram_model[word1, word2][word3] = freq/self.bigram_counter[word1, word2]

    def serialize(self):
        current_path = os.getcwd()
        with open(current_path + '\\models\\' + 'terms.p', 'wb') as open_file:
            pickle.dump(self.term_counter, open_file, -1)

        with open(current_path + '\\models\\' + 'bigrams.p', 'wb') as open_file:
            pickle.dump(self.bigram_counter, open_file, -1)

        with open(current_path + '\\models\\' + 'trigrams.p', 'wb') as open_file:
            pickle.dump(self.trigram_counter, open_file, -1)


if __name__ == '__main__':
    with profiler() as p:
        trainer = Trainer(3)
        trainer.get_counters_from_file()
        print("Terms - {0}, bigrams - {1}, trigrams - {2}".format(len(trainer.term_counter.keys()),
                                                                  len(trainer.bigram_counter.keys()),
                                                                  len(trainer.trigram_counter.keys())))
        #trainer.filter_dicts()
        #for bigram in trainer.bigram_model.items():
         #   print(bigram)
        #for trigram in trainer.trigram_model.items():
            #print(trigram)
        trainer.serialize()
        #trainer.filter_dicts()
        print("Terms - {0}, bigrams - {1}, trigrams - {2}".format(len(trainer.term_counter.keys()),
                                                                  len(trainer.bigram_counter.keys()),
                                                                  len(trainer.trigram_counter.keys())))

