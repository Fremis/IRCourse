import os
from collections import *
import time
import json
import re
import pickle

import gc


class profiler(object):
    def __enter__(self):
        self._startTime = time.clock()

    def __exit__(self, type, value, traceback):
        print("Elapsed time: {:.8f} sec".format(time.clock() - self._startTime))


class Trainer:
    def __init__(self, n):
        self.term_counter = defaultdict(int)
        self.bigram_counter = defaultdict(int)
        self.n = n
        self.bigram_model = defaultdict(dict)
        self.extra_symbols = re.compile("[^\w\'\"-]+", re.UNICODE)

    def get_grams(self, tokens):
        for i in range(len(tokens) - (self.n - 1)):
            bigram = tuple(tokens[i:i + self.n - 1])
            yield bigram

    def get_counters_from_files(self):
        current_path = os.getcwd()
        tokens = []
        file_list = os.listdir(current_path + '\\index\\')
        for file in file_list:
            if file:
                filename = current_path + '\\index\\' + file
                self.train_from_json(filename)
            print('file parsed')
        self.serialize()
        gc.collect()
        print('files serialized')
        print('Folder parsed')
        # self.build_model()

    def train_from_json(self, filename):
        body_tokens = []
        with open(filename, 'r', encoding='utf-8') as f:
            docs = json.load(f)
            for doc in docs:
                description = doc['description']
                if description:
                    body_tokens = list(filter(lambda x: len(x) > 0,
                                              (re.sub(self.extra_symbols, '', token.lower())
                                               for token in description.split())))
                title_tokens = list(filter(lambda x: len(x) > 0,
                                           (re.sub(self.extra_symbols, '', token.lower())
                                            for token in doc['title'].split())))
                self.train(body_tokens, title_tokens)

    def train(self, body_tokens, title_tokens):
        if body_tokens:
            for token in body_tokens:
                self.term_counter[token] += 1
        for token in title_tokens:
            self.term_counter[token] += 20
        for bigram in self.get_grams(title_tokens):
            self.bigram_counter[bigram] += 20

    def build_model(self):
        for (word1, word2), freq in self.bigram_counter.items():
            self.bigram_model[word1][word2] = freq / self.term_counter[word1]

    def serialize(self):
        current_path = os.getcwd()
        with open(current_path + '\\models\\' + 'terms.p', 'wb') as open_file:
            pickle.dump(self.term_counter, open_file, -1)

        with open(current_path + '\\models\\' + 'bigrams.p', 'wb') as open_file:
            pickle.dump(self.bigram_counter, open_file, -1)


if __name__ == '__main__':
    with profiler() as p:
        trainer = Trainer(3)
        trainer.get_counters_from_files()
    print(trainer.bigram_counter[('sonic', 'adventure')])
    print(len(trainer.term_counter.keys()))
    print(len(trainer.bigram_counter.keys()))
    while True:
        input()
        print(trainer.bigram_counter[(tuple(input().split(' ')))])
