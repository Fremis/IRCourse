import re
import os
from collections import *
import pickle

import time



class Corrector:

    alphabet_cyr = 'абвгдеёжзийклмнопрстуфхцчшщъыьэюя'
    alphabet_lat = 'abcdefghijklmnopqrstuvwxyz'
    translit_lat_cyr = {'q': 'й', 'w': 'ц', 'e': 'у', 'r': 'к', 't': 'е', 'y': 'н', 'u': 'г', 'i': 'ш', 'o': 'щ',
                        'p': 'з',
                        '[': 'х', ']': 'ъ',
                        'a': 'ф', 's': 'ы', 'd': 'в', 'f': 'а', 'g': 'п', 'h': 'р', 'j': 'о', 'k': 'л', 'l': 'д',
                        ';': 'ж',
                        '\'': 'э',
                        'z': 'я', 'x': 'ч', 'c': 'с', 'v': 'м', 'b': 'и', 'n': 'т', 'm': 'ь', ',': 'б', '.': 'ю',
                        '`': 'ё'}

    translit_cyr_lat = dict((v, k) for k, v in translit_lat_cyr.items())

    def __init__(self):
        try:
            self.alphabet = ''
            current_path = os.getcwd()
            with open(current_path + '\\models\\' + 'terms.p', 'rb') as open_file:
                self.terms = pickle.load(open_file)
            with open(current_path + '\\models\\' + 'bigrams.p', 'rb') as open_file:
                self.bigrams = pickle.load(open_file)
            with open(current_path + '\\models\\' + 'trigrams.p', 'rb') as open_file:
                self.trigrams = pickle.load(open_file)

            print('index deserialized')
        except FileNotFoundError:
            print('spellchecker is not trained yet, lvl it up!')

    def edits1(self, word):
        splits = [(word[:i], word[i:]) for i in range(len(word) + 1)]
        deletes = [a + b[1:] for a, b in splits if b]
        transposes = [a + b[1] + b[0] + b[2:] for a, b in splits if len(b) > 1]
        replaces = [a + c + b[1:] for a, b in splits for c in self.alphabet if b]
        inserts = [a + c + b for a, b in splits for c in self.alphabet]
        return set(deletes + transposes + replaces + inserts)

    def known_edits2(self, word):
        return set(e2 for e1 in self.edits1(word) for e2 in self.edits1(e1) if e2 in self.terms)

    def known_edits2_pairs(self, word):
        return set(e2 for e1 in self.edits1(word) for e2 in self.edits1(e1) if e2 in self.bigrams)

    def known_edits2_trigrams(self, word):
        return set(e2 for e1 in self.edits1(word) for e2 in self.edits1(e1) if e2 in self.trigrams)

    def known(self, words):
        return set(w for w in words if w in self.terms)

    def known_pairs(self, words):
        return set(w for w in words if w in self.bigrams)

    def known_trigrams(self, words):
        return set(w for w in words if w in self.trigrams)

    def query_to_bigrams(self, word):
        return [' '.join((word[i], word[i + 1])) for i in range(len(word) - 1)]

    def translit_cure(self, word, mode):
        if mode == 'lat':
            corrected = ''
            for i in range(len(word)):
                if word[i] in self.translit_cyr_lat:
                    corrected += self.translit_cyr_lat[word[i]]
                else:
                    corrected += word[i]
            return corrected
        if mode == 'cyr':
            corrected = ''
            for i in range(len(word)):
                if word[i] in self.translit_lat_cyr:
                    corrected += self.translit_lat_cyr[word[i]]
                else:
                    corrected += word[i]
            return corrected

    def get_approx_lang(self, word):
        if re.findall('[а-я0-9]+', word):
            self.alphabet = self.alphabet_cyr
            return 'lat'
        else:
            self.alphabet = self.alphabet_lat
            return 'cyr'


    def correct(self, word):
        word = word.lower()
        lang = self.get_approx_lang(word)
        query_to_list = word.split(' ')

        if len(word.split(' ')) > 3:
            corrected = ''
            while len(query_to_list) >= 3:
                corrected += ' ' + self.correct(' '.join([query_to_list.pop(0) for _ in range(3)]))
            corrected += ' '
            if len(query_to_list) > 1:
                corrected += self.correct(' '.join([query_to_list.pop(0) for _ in range(2)]))
            elif len(query_to_list) == 1:
                corrected += self.correct(query_to_list.pop(0))
            return corrected.strip()

        elif len(word.split(' ')) == 3:
            candidates = self.known_trigrams([word]) or self.known_trigrams(self.edits1(word)) or \
                         self.known_edits2_trigrams(word) or self.known_trigrams([self.translit_cure(word, mode=lang)]) \
                         or [word]
            if candidates == [word]:
                corrected = self.correct(' '.join([query_to_list.pop(0) for _ in range(2)])) + \
                            ' ' + self.correct(query_to_list.pop(0))
                return corrected
            print(candidates)
            return max(candidates, key=self.trigrams.get)
        elif len(word.split(' ')) == 2:
            candidates = self.known_pairs([word]) or self.known_pairs(self.edits1(word)) or self.known_edits2_pairs(
                word) or self.known_pairs([self.translit_cure(word, mode=lang)]) or [word]
            print(candidates)
            return max(candidates, key=self.bigrams.get)
        else:
            candidates = self.known([word]) or self.known(self.edits1(word)) or self.known_edits2(word) or\
                         self.known([self.translit_cure(word, mode=lang)]) or [word]
            print(len(candidates))
            print(candidates)
            return max(candidates, key=self.terms.get)


class Profiler(object):
    def __enter__(self):
        self._startTime = time.clock()

    def __exit__(self, type, value, traceback):
        print("Elapsed time: {:.8f} sec".format(time.clock() - self._startTime))


'''
if __name__ == '__main__':
    with Profiler() as p:
        corrector = Corrector()

        print(len(corrector.terms.keys()))
    print('rdy to input')
    while True:
            user_input = input()
            with Profiler() as p:
                print(corrector.correct(user_input))
'''